// Fill out your copyright notice in the Description page of Project Settings.


#include "MediaActor.h"

#include "MediaPlayer.h"

// Sets default values
AMediaActor::AMediaActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMediaActor::BeginPlay()
{
	Super::BeginPlay();


	//2.打开媒体资源
	mMediaPlayer->OpenFile(TEXT("/Game/Movies/p01.pm4"));
	//播放模式
	mMediaPlayer->SetLooping(true);//循环播放


	/*
	UMediaSource* SampleVide = LoadObject<UMediaSource>(NULL, TEXT("FileMediaSource'/Game/Movies/SampleVideo.SampleVideo'"));

	
	mediaPlayer = LoadObject<UMediaPlayer>(NULL, TEXT("MediaPlayer'/Game/Movies/SampleMedia.SampleMedia'"));
	mediaPlayer->OpenSource(SampleVide);
	*/
}

// Called every frame
void AMediaActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

