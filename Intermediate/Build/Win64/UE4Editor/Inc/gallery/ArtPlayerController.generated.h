// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GALLERY_ArtPlayerController_generated_h
#error "ArtPlayerController.generated.h already included, missing '#pragma once' in ArtPlayerController.h"
#endif
#define GALLERY_ArtPlayerController_generated_h

#define gallery_Source_gallery_ArtPlayerController_h_15_SPARSE_DATA
#define gallery_Source_gallery_ArtPlayerController_h_15_RPC_WRAPPERS
#define gallery_Source_gallery_ArtPlayerController_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define gallery_Source_gallery_ArtPlayerController_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAArtPlayerController(); \
	friend struct Z_Construct_UClass_AArtPlayerController_Statics; \
public: \
	DECLARE_CLASS(AArtPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/gallery"), NO_API) \
	DECLARE_SERIALIZER(AArtPlayerController)


#define gallery_Source_gallery_ArtPlayerController_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAArtPlayerController(); \
	friend struct Z_Construct_UClass_AArtPlayerController_Statics; \
public: \
	DECLARE_CLASS(AArtPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/gallery"), NO_API) \
	DECLARE_SERIALIZER(AArtPlayerController)


#define gallery_Source_gallery_ArtPlayerController_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AArtPlayerController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AArtPlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AArtPlayerController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AArtPlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AArtPlayerController(AArtPlayerController&&); \
	NO_API AArtPlayerController(const AArtPlayerController&); \
public:


#define gallery_Source_gallery_ArtPlayerController_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AArtPlayerController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AArtPlayerController(AArtPlayerController&&); \
	NO_API AArtPlayerController(const AArtPlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AArtPlayerController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AArtPlayerController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AArtPlayerController)


#define gallery_Source_gallery_ArtPlayerController_h_15_PRIVATE_PROPERTY_OFFSET
#define gallery_Source_gallery_ArtPlayerController_h_12_PROLOG
#define gallery_Source_gallery_ArtPlayerController_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gallery_Source_gallery_ArtPlayerController_h_15_PRIVATE_PROPERTY_OFFSET \
	gallery_Source_gallery_ArtPlayerController_h_15_SPARSE_DATA \
	gallery_Source_gallery_ArtPlayerController_h_15_RPC_WRAPPERS \
	gallery_Source_gallery_ArtPlayerController_h_15_INCLASS \
	gallery_Source_gallery_ArtPlayerController_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gallery_Source_gallery_ArtPlayerController_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gallery_Source_gallery_ArtPlayerController_h_15_PRIVATE_PROPERTY_OFFSET \
	gallery_Source_gallery_ArtPlayerController_h_15_SPARSE_DATA \
	gallery_Source_gallery_ArtPlayerController_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	gallery_Source_gallery_ArtPlayerController_h_15_INCLASS_NO_PURE_DECLS \
	gallery_Source_gallery_ArtPlayerController_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GALLERY_API UClass* StaticClass<class AArtPlayerController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gallery_Source_gallery_ArtPlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
