// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GALLERY_MediaActor_generated_h
#error "MediaActor.generated.h already included, missing '#pragma once' in MediaActor.h"
#endif
#define GALLERY_MediaActor_generated_h

#define gallery_Source_gallery_MediaActor_h_14_SPARSE_DATA
#define gallery_Source_gallery_MediaActor_h_14_RPC_WRAPPERS
#define gallery_Source_gallery_MediaActor_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define gallery_Source_gallery_MediaActor_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMediaActor(); \
	friend struct Z_Construct_UClass_AMediaActor_Statics; \
public: \
	DECLARE_CLASS(AMediaActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/gallery"), NO_API) \
	DECLARE_SERIALIZER(AMediaActor)


#define gallery_Source_gallery_MediaActor_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAMediaActor(); \
	friend struct Z_Construct_UClass_AMediaActor_Statics; \
public: \
	DECLARE_CLASS(AMediaActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/gallery"), NO_API) \
	DECLARE_SERIALIZER(AMediaActor)


#define gallery_Source_gallery_MediaActor_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMediaActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMediaActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMediaActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMediaActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMediaActor(AMediaActor&&); \
	NO_API AMediaActor(const AMediaActor&); \
public:


#define gallery_Source_gallery_MediaActor_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMediaActor(AMediaActor&&); \
	NO_API AMediaActor(const AMediaActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMediaActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMediaActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMediaActor)


#define gallery_Source_gallery_MediaActor_h_14_PRIVATE_PROPERTY_OFFSET
#define gallery_Source_gallery_MediaActor_h_11_PROLOG
#define gallery_Source_gallery_MediaActor_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gallery_Source_gallery_MediaActor_h_14_PRIVATE_PROPERTY_OFFSET \
	gallery_Source_gallery_MediaActor_h_14_SPARSE_DATA \
	gallery_Source_gallery_MediaActor_h_14_RPC_WRAPPERS \
	gallery_Source_gallery_MediaActor_h_14_INCLASS \
	gallery_Source_gallery_MediaActor_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gallery_Source_gallery_MediaActor_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gallery_Source_gallery_MediaActor_h_14_PRIVATE_PROPERTY_OFFSET \
	gallery_Source_gallery_MediaActor_h_14_SPARSE_DATA \
	gallery_Source_gallery_MediaActor_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	gallery_Source_gallery_MediaActor_h_14_INCLASS_NO_PURE_DECLS \
	gallery_Source_gallery_MediaActor_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GALLERY_API UClass* StaticClass<class AMediaActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gallery_Source_gallery_MediaActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
