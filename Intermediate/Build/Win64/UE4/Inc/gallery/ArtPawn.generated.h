// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GALLERY_ArtPawn_generated_h
#error "ArtPawn.generated.h already included, missing '#pragma once' in ArtPawn.h"
#endif
#define GALLERY_ArtPawn_generated_h

#define gallery_Source_gallery_ArtPawn_h_12_SPARSE_DATA
#define gallery_Source_gallery_ArtPawn_h_12_RPC_WRAPPERS
#define gallery_Source_gallery_ArtPawn_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define gallery_Source_gallery_ArtPawn_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAArtPawn(); \
	friend struct Z_Construct_UClass_AArtPawn_Statics; \
public: \
	DECLARE_CLASS(AArtPawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/gallery"), NO_API) \
	DECLARE_SERIALIZER(AArtPawn)


#define gallery_Source_gallery_ArtPawn_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAArtPawn(); \
	friend struct Z_Construct_UClass_AArtPawn_Statics; \
public: \
	DECLARE_CLASS(AArtPawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/gallery"), NO_API) \
	DECLARE_SERIALIZER(AArtPawn)


#define gallery_Source_gallery_ArtPawn_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AArtPawn(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AArtPawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AArtPawn); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AArtPawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AArtPawn(AArtPawn&&); \
	NO_API AArtPawn(const AArtPawn&); \
public:


#define gallery_Source_gallery_ArtPawn_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AArtPawn(AArtPawn&&); \
	NO_API AArtPawn(const AArtPawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AArtPawn); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AArtPawn); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AArtPawn)


#define gallery_Source_gallery_ArtPawn_h_12_PRIVATE_PROPERTY_OFFSET
#define gallery_Source_gallery_ArtPawn_h_9_PROLOG
#define gallery_Source_gallery_ArtPawn_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gallery_Source_gallery_ArtPawn_h_12_PRIVATE_PROPERTY_OFFSET \
	gallery_Source_gallery_ArtPawn_h_12_SPARSE_DATA \
	gallery_Source_gallery_ArtPawn_h_12_RPC_WRAPPERS \
	gallery_Source_gallery_ArtPawn_h_12_INCLASS \
	gallery_Source_gallery_ArtPawn_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gallery_Source_gallery_ArtPawn_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gallery_Source_gallery_ArtPawn_h_12_PRIVATE_PROPERTY_OFFSET \
	gallery_Source_gallery_ArtPawn_h_12_SPARSE_DATA \
	gallery_Source_gallery_ArtPawn_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	gallery_Source_gallery_ArtPawn_h_12_INCLASS_NO_PURE_DECLS \
	gallery_Source_gallery_ArtPawn_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GALLERY_API UClass* StaticClass<class AArtPawn>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gallery_Source_gallery_ArtPawn_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
