// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "gallery/ArtPawn.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeArtPawn() {}
// Cross Module References
	GALLERY_API UClass* Z_Construct_UClass_AArtPawn_NoRegister();
	GALLERY_API UClass* Z_Construct_UClass_AArtPawn();
	ENGINE_API UClass* Z_Construct_UClass_APawn();
	UPackage* Z_Construct_UPackage__Script_gallery();
// End Cross Module References
	void AArtPawn::StaticRegisterNativesAArtPawn()
	{
	}
	UClass* Z_Construct_UClass_AArtPawn_NoRegister()
	{
		return AArtPawn::StaticClass();
	}
	struct Z_Construct_UClass_AArtPawn_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AArtPawn_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APawn,
		(UObject* (*)())Z_Construct_UPackage__Script_gallery,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArtPawn_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "ArtPawn.h" },
		{ "ModuleRelativePath", "ArtPawn.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AArtPawn_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AArtPawn>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AArtPawn_Statics::ClassParams = {
		&AArtPawn::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AArtPawn_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AArtPawn_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AArtPawn()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AArtPawn_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AArtPawn, 4074815042);
	template<> GALLERY_API UClass* StaticClass<AArtPawn>()
	{
		return AArtPawn::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AArtPawn(Z_Construct_UClass_AArtPawn, &AArtPawn::StaticClass, TEXT("/Script/gallery"), TEXT("AArtPawn"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AArtPawn);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
