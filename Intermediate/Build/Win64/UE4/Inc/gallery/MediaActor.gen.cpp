// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "gallery/MediaActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMediaActor() {}
// Cross Module References
	GALLERY_API UClass* Z_Construct_UClass_AMediaActor_NoRegister();
	GALLERY_API UClass* Z_Construct_UClass_AMediaActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_gallery();
	MEDIAASSETS_API UClass* Z_Construct_UClass_UMediaPlayer_NoRegister();
// End Cross Module References
	void AMediaActor::StaticRegisterNativesAMediaActor()
	{
	}
	UClass* Z_Construct_UClass_AMediaActor_NoRegister()
	{
		return AMediaActor::StaticClass();
	}
	struct Z_Construct_UClass_AMediaActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_mMediaPlayer_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_mMediaPlayer;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMediaActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_gallery,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMediaActor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MediaActor.h" },
		{ "ModuleRelativePath", "MediaActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMediaActor_Statics::NewProp_mMediaPlayer_MetaData[] = {
		{ "Category", "PlayMedia" },
		{ "Comment", "//********************************Media*******************************//\n//\xc3\xbd?\xe5\xb2\xa5????(?\xe0\xbc\xad????\xd6\xb8??)\n" },
		{ "ModuleRelativePath", "MediaActor.h" },
		{ "ToolTip", "****************************Media******************************/\n\xc3\xbd?\xe5\xb2\xa5????(?\xe0\xbc\xad????\xd6\xb8??)" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMediaActor_Statics::NewProp_mMediaPlayer = { "mMediaPlayer", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMediaActor, mMediaPlayer), Z_Construct_UClass_UMediaPlayer_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMediaActor_Statics::NewProp_mMediaPlayer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMediaActor_Statics::NewProp_mMediaPlayer_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMediaActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMediaActor_Statics::NewProp_mMediaPlayer,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMediaActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMediaActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMediaActor_Statics::ClassParams = {
		&AMediaActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AMediaActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AMediaActor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMediaActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMediaActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMediaActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMediaActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMediaActor, 631077610);
	template<> GALLERY_API UClass* StaticClass<AMediaActor>()
	{
		return AMediaActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMediaActor(Z_Construct_UClass_AMediaActor, &AMediaActor::StaticClass, TEXT("/Script/gallery"), TEXT("AMediaActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMediaActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
